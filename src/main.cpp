#include <WiFi.h>
#include <Web3.h>
#include <Util.h>
#include <Contract.h>

const char *ssid = "Qryptos2";
const char *password = "Morrowind";
#define MY_ADDRESS "0x[WalletAddress]"      //Put your wallet address here
#define INFURA_HOST "kovan.infura.io"
#define INFURA_PATH "/v3/[Infura Key]"           //please change this Infura key to your own once you have the sample running
#define TARGETADDRESS "0xD9ba5B381d894956710fAb06e462D59e339Dd244"   //put your second address here
#define ETHERSCAN_TX "https://kovan.etherscan.io/tx/"

// Copy/paste the private key in here
#define PRIVATE_KEY "0x[PrivateKey]" //32 Byte Private key

Web3 web3(INFURA_HOST, INFURA_PATH);

void setupWifi();
void sendEthToAddress(double eths);

void setup() {
    Serial.begin(9600);
    setupWifi();
    sendEthToAddress(0.001);
}

void loop() {

}

void setupWifi()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        return;
    }

    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    if (WiFi.status() != WL_CONNECTED)
    {
        WiFi.persistent(false);
        WiFi.mode(WIFI_OFF);
        WiFi.mode(WIFI_STA);

        WiFi.begin(ssid, password);
    }

    int wificounter = 0;
    while (WiFi.status() != WL_CONNECTED && wificounter < 10)
    {
        for (int i = 0; i < 500; i++)
        {
            delay(1);
        }
        Serial.print(".");
        wificounter++;
    }

    if (wificounter >= 10)
    {
        Serial.println("Restarting ...");
        ESP.restart(); //targetting 8266 & Esp32 - you may need to replace this
    }

    delay(10);

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void sendEthToAddress(double eth)
{
   //some operation for send ETH to address
}